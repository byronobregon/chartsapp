$(document).on('turbolinks:load', function () {
  $(charts.setup())
})

var charts = {
  setup: function () {
    $('.tagsinput').tagsinput({
      tagClass: 'label label-primary'
    })

    $('#chartName').on('focusout', function () {
      charts.updateChartName($(this).html())
    })

    $('#xValueInput').on('change', function () {
      charts.createXvalues($(this))
    })

    $('#dataCenters').on('focusout', '.center-value', function () {
      charts.updateCenterValue($(this))
    })

    $('#dataCenters').on('focus', '.center-value', function () {
      var cell = this;
      // select all text in contenteditable
      // see http://stackoverflow.com/a/6150060/145346
      var range, selection;
      if (document.body.createTextRange) {
        range = document.body.createTextRange();
        range.moveToElementText(cell);
        range.select();
      } else if (window.getSelection) {
        selection = window.getSelection();
        range = document.createRange();
        range.selectNodeContents(cell);
        selection.removeAllRanges();
        selection.addRange(range);
      }
    })

    $('.display-chart').on('click', function () {
      charts.displayChart($(this))
    })

    $('#chart_type').on('change', function () {
      charts.updateChartType($(this).val())
    })
  },

  updateChartName: function (value) {
    var pathArray = window.location.pathname.split('/')
    var chart = pathArray[2]

    $.ajax({
      url: '/charts/' + chart,
      type: 'patch',
      dataType: 'json',
      data: {
        chart: {
          name: value
        }
      },
      success: function () {
        toastr.success('Gráfico Actualizado')
      }
    })
  },

  createXvalues: function (field) {
    var pathArray = window.location.pathname.split('/')
    var chart = pathArray[2]

    $.ajax({
      url: '/charts/' + chart + '/x_values',
      type: 'post',
      dataType: 'script',
      data: {
        x_values: field.val()
      }
    })
  },

  updateCenterValue: function (field) {
    var pathArray = window.location.pathname.split('/')
    var chart = pathArray[2]
    var id = field.attr('data-id')

    $.ajax({
      url: '/charts/' + chart + '/center_values/' + id,
      type: 'patch',
      dataType: 'script',
      data: {
        center_value: {
          value: field.html()
        }
      }
    })
  },

  displayChart: function (button) {
    var id = button.attr('data-id')

    $.ajax({
      url: '/charts',
      type: 'get',
      dataType: 'script',
      data: {
        chart_id: id
      }
    })
  },

  updateChartType: function (value) {
    var pathArray = window.location.pathname.split('/')
    var chart = pathArray[2]

    $.ajax({
      url: '/charts/' + chart,
      type: 'patch',
      dataType: 'script',
      data: {
        chart: {
          chart_type: value
        }
      }
    })
  }
}