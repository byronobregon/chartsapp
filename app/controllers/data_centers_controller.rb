# frozen_string_literal: true

class DataCentersController < ApplicationController
  before_action :set_data_center, only: %i[edit update destroy]
  before_action :set_chart

  load_and_authorize_resource :chart
  load_and_authorize_resource :data_center, through: :chart

  def new
    @data_center = DataCenter.new
  end

  def edit; end

  def create
    @data_center = DataCenter.new(data_center_params)
    @data_center.chart = @chart

    respond_to do |format|
      if @data_center.save
        format.html { redirect_to edit_chart_path(@chart), notice: 'Centro de Datos Creado' }
      else
        format.html { render :new }
      end
    end
  end

  def update
    respond_to do |format|
      if @data_center.update(data_center_params)
        format.html { redirect_to edit_chart_path(@chart), notice: 'Centro de Datos Actualizado' }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    @data_center.destroy
    respond_to do |format|
      format.html { redirect_to edit_chart_path(@chart), notice: 'Centro de Datos Eliminado' }
    end
  end

  private

  def set_chart
    @chart = Chart.find(params[:chart_id])
  end

  def set_data_center
    @data_center = DataCenter.find(params[:id])
  end

  def data_center_params
    params.require(:data_center).permit(:name, :color)
  end
end
