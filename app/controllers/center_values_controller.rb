# frozen_string_literal: true

class CenterValuesController < ApplicationController
  before_action :set_center_value
  before_action :set_chart

  load_and_authorize_resource :chart
  load_and_authorize_resource :x_value, through: :chart
  load_and_authorize_resource :center_value, through: :x_value

  def update
    respond_to do |format|
      if @center_value.update(center_value_params)
        format.js
      end
    end
  end


  private

  def set_chart
    @chart = Chart.find(params[:chart_id])
  end

  def set_center_value
    @center_value = CenterValue.find(params[:id])
  end

  def center_value_params
    params.require(:center_value).permit(:value)
  end
end
