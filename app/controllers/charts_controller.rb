# frozen_string_literal: true

class ChartsController < ApplicationController
  before_action :set_chart, only: %i[edit update destroy]

  load_and_authorize_resource

  def index
    @charts = current_user.charts
    @chart = if params[:chart_id]
               Chart.find(params[:chart_id])
             else
              @charts.first
             end

    respond_to do |f|
      f.html
      f.js
    end
  end

  def new
    @chart = Chart.new
  end

  def edit; end

  def create
    @chart = Chart.new(chart_params)
    @chart.user = current_user

    respond_to do |format|
      if @chart.save
        format.html { redirect_to edit_chart_path(@chart), notice: 'Gráfico Creado' }
      else
        format.html { redirect_to new_chart_path, alert: 'Se debe Ingresar un Nombre' }
      end
    end
  end

  def update
    respond_to do |format|
      if @chart.update(chart_params)
        format.json { render json: @chart }
        format.js
      end
    end
  end

  def destroy
    @chart.destroy
    respond_to do |format|
      format.html { redirect_to charts_url, notice: 'Gráfico Eliminado' }
    end
  end

  private

  def set_chart
    @chart = Chart.find(params[:id])
  end

  def chart_params
    params.require(:chart).permit(:name, :chart_type)
  end
end
