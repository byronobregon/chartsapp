# frozen_string_literal: true

class XValuesController < ApplicationController
  before_action :set_chart

  load_and_authorize_resource :chart
  load_and_authorize_resource :x_value, through: :chart

  def create
    @chart.associate_x_values(params[:x_values])

    respond_to do |f|
      f.js
    end
  end

  private

  def set_chart
    @chart = Chart.find(params[:chart_id])
  end
end
