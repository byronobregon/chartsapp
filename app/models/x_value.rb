class XValue < ApplicationRecord
  belongs_to :chart

  has_many :center_values, dependent: :destroy

  after_create :associate_center_values

  default_scope { order(id: :asc) }

  private

  def associate_center_values
    chart.data_centers.each do |dc|
      CenterValue.create(data_center_id: dc.id, x_value_id: id, value: 0)
    end
  end
end
