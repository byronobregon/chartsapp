class CenterValue < ApplicationRecord
  belongs_to :x_value
  belongs_to :data_center

  default_scope { order(id: :asc) }

  before_save :sanitize_value

  validates :value, presence: true

  private

  def sanitize_value
    self.value = value.tr('<>divbr\/', '')

    return unless value == ''

    self.value = 0
  end
end
