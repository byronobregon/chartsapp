class DataCenter < ApplicationRecord
  belongs_to :chart

  has_many :center_values, dependent: :destroy

  validates :name, presence: true
  validates :color, presence: true
  validate :chart_type

  after_create :associate_x_values

  default_scope { order(id: :asc) }

  private

  def associate_x_values
    chart.x_values.each do |x|
      CenterValue.create(x_value_id: x.id, data_center_id: id, value: 0)
    end
  end

  def chart_type
    incorrect_types = %w[piev2]
    return unless incorrect_types.include?(chart.chart_type) && chart.data_centers.length > 1

    errors.add(:Error, 'Este Tipo de Gráfico sólo acepta un Centro de Datos')
  end
end
