class Chart < ApplicationRecord
  belongs_to :user

  has_many :x_values, dependent: :destroy
  has_many :data_centers, dependent: :destroy

  default_scope { order(id: :desc) }

  validates :name, presence: true

  enum chart_type: %i[line bar radar barv2 barv3 piev2]

  def associate_x_values(values_string)
    new_values = values_string.split(',')
    x_values.where.not(name: new_values).each(&:destroy)
    actual_values = x_values.pluck(:name)

    new_values.each { |value| XValue.create(name: value, chart_id: id) unless actual_values.include?(value) }
  end

  def select_type_options
    if data_centers.length > 1
      [['Gráfico de Líneas', 'line'], ['Gráfico de Barras', 'bar'], ['Gráfico Radar', 'radar'], ['Gráfico de Barras V2', 'barv2'], ['Gráfico de Barras V3', 'barv3']]
    else
      [['Gráfico de Líneas', 'line'], ['Gráfico de Barras', 'bar'], ['Gráfico Radar', 'radar'], ['Gráfico de Barras V2', 'barv2'], ['Gráfico de Barras V3', 'barv3'], ['Gráfico de Pie', 'piev2']]
    end
  end
end
