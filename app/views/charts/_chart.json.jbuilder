json.extract! chart, :id, :name, :user_id, :created_at, :updated_at
json.url chart_url(chart, format: :json)
