json.extract! data_center, :id, :name, :color, :chart_id, :created_at, :updated_at
json.url data_center_url(data_center, format: :json)
