class AddTypeToChart < ActiveRecord::Migration[5.2]
  def change
    add_column :charts, :chart_type, :integer, default: 0
  end
end
