class CreateDataCenters < ActiveRecord::Migration[5.2]
  def change
    create_table :data_centers do |t|
      t.string :name
      t.string :color
      t.references :chart, foreign_key: true

      t.timestamps
    end
  end
end
