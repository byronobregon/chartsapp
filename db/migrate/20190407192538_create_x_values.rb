class CreateXValues < ActiveRecord::Migration[5.2]
  def change
    create_table :x_values do |t|
      t.string :name
      t.references :chart, foreign_key: true

      t.timestamps
    end
  end
end
