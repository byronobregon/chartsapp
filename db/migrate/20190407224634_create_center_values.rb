class CreateCenterValues < ActiveRecord::Migration[5.2]
  def change
    create_table :center_values do |t|
      t.string :value
      t.references :x_value, foreign_key: true
      t.references :data_center, foreign_key: true

      t.timestamps
    end
  end
end
