require "application_system_test_case"

class DataCentersTest < ApplicationSystemTestCase
  setup do
    @data_center = data_centers(:one)
  end

  test "visiting the index" do
    visit data_centers_url
    assert_selector "h1", text: "Data Centers"
  end

  test "creating a Data center" do
    visit data_centers_url
    click_on "New Data Center"

    fill_in "Chart", with: @data_center.chart_id
    fill_in "Color", with: @data_center.color
    fill_in "Name", with: @data_center.name
    click_on "Create Data center"

    assert_text "Data center was successfully created"
    click_on "Back"
  end

  test "updating a Data center" do
    visit data_centers_url
    click_on "Edit", match: :first

    fill_in "Chart", with: @data_center.chart_id
    fill_in "Color", with: @data_center.color
    fill_in "Name", with: @data_center.name
    click_on "Update Data center"

    assert_text "Data center was successfully updated"
    click_on "Back"
  end

  test "destroying a Data center" do
    visit data_centers_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Data center was successfully destroyed"
  end
end
