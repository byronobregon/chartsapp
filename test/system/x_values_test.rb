require "application_system_test_case"

class XValuesTest < ApplicationSystemTestCase
  setup do
    @x_value = x_values(:one)
  end

  test "visiting the index" do
    visit x_values_url
    assert_selector "h1", text: "X Values"
  end

  test "creating a X value" do
    visit x_values_url
    click_on "New X Value"

    fill_in "Chart", with: @x_value.chart_id
    fill_in "Name", with: @x_value.name
    click_on "Create X value"

    assert_text "X value was successfully created"
    click_on "Back"
  end

  test "updating a X value" do
    visit x_values_url
    click_on "Edit", match: :first

    fill_in "Chart", with: @x_value.chart_id
    fill_in "Name", with: @x_value.name
    click_on "Update X value"

    assert_text "X value was successfully updated"
    click_on "Back"
  end

  test "destroying a X value" do
    visit x_values_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "X value was successfully destroyed"
  end
end
