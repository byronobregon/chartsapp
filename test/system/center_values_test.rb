require "application_system_test_case"

class CenterValuesTest < ApplicationSystemTestCase
  setup do
    @center_value = center_values(:one)
  end

  test "visiting the index" do
    visit center_values_url
    assert_selector "h1", text: "Center Values"
  end

  test "creating a Center value" do
    visit center_values_url
    click_on "New Center Value"

    fill_in "Data center", with: @center_value.data_center_id
    fill_in "Value", with: @center_value.value
    fill_in "X values", with: @center_value.x_values_id
    click_on "Create Center value"

    assert_text "Center value was successfully created"
    click_on "Back"
  end

  test "updating a Center value" do
    visit center_values_url
    click_on "Edit", match: :first

    fill_in "Data center", with: @center_value.data_center_id
    fill_in "Value", with: @center_value.value
    fill_in "X values", with: @center_value.x_values_id
    click_on "Update Center value"

    assert_text "Center value was successfully updated"
    click_on "Back"
  end

  test "destroying a Center value" do
    visit center_values_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Center value was successfully destroyed"
  end
end
