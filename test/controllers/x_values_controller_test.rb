require 'test_helper'

class XValuesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @x_value = x_values(:one)
  end

  test "should get index" do
    get x_values_url
    assert_response :success
  end

  test "should get new" do
    get new_x_value_url
    assert_response :success
  end

  test "should create x_value" do
    assert_difference('XValue.count') do
      post x_values_url, params: { x_value: { chart_id: @x_value.chart_id, name: @x_value.name } }
    end

    assert_redirected_to x_value_url(XValue.last)
  end

  test "should show x_value" do
    get x_value_url(@x_value)
    assert_response :success
  end

  test "should get edit" do
    get edit_x_value_url(@x_value)
    assert_response :success
  end

  test "should update x_value" do
    patch x_value_url(@x_value), params: { x_value: { chart_id: @x_value.chart_id, name: @x_value.name } }
    assert_redirected_to x_value_url(@x_value)
  end

  test "should destroy x_value" do
    assert_difference('XValue.count', -1) do
      delete x_value_url(@x_value)
    end

    assert_redirected_to x_values_url
  end
end
