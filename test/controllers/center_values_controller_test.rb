require 'test_helper'

class CenterValuesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @center_value = center_values(:one)
  end

  test "should get index" do
    get center_values_url
    assert_response :success
  end

  test "should get new" do
    get new_center_value_url
    assert_response :success
  end

  test "should create center_value" do
    assert_difference('CenterValue.count') do
      post center_values_url, params: { center_value: { data_center_id: @center_value.data_center_id, value: @center_value.value, x_values_id: @center_value.x_values_id } }
    end

    assert_redirected_to center_value_url(CenterValue.last)
  end

  test "should show center_value" do
    get center_value_url(@center_value)
    assert_response :success
  end

  test "should get edit" do
    get edit_center_value_url(@center_value)
    assert_response :success
  end

  test "should update center_value" do
    patch center_value_url(@center_value), params: { center_value: { data_center_id: @center_value.data_center_id, value: @center_value.value, x_values_id: @center_value.x_values_id } }
    assert_redirected_to center_value_url(@center_value)
  end

  test "should destroy center_value" do
    assert_difference('CenterValue.count', -1) do
      delete center_value_url(@center_value)
    end

    assert_redirected_to center_values_url
  end
end
