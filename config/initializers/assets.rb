# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path.
# Rails.application.config.assets.paths << Emoji.images_path
# Add Yarn node_modules folder to the asset load path.
Rails.application.config.assets.paths << Rails.root.join('node_modules')
Rails.application.config.assets.paths << Rails.root.join('vendor', 'assets', 'css')
Rails.application.config.assets.paths << Rails.root.join('vendor', 'assets', 'css', 'patterns')
Rails.application.config.assets.paths << Rails.root.join('vendor', 'assets', 'css', 'plugins')
Rails.application.config.assets.paths << Rails.root.join('vendor', 'assets', 'css', 'plugins', 'toastr')
Rails.application.config.assets.paths << Rails.root.join('vendor', 'assets', 'css', 'plugins', 'bootstrap-tagsinput')
Rails.application.config.assets.paths << Rails.root.join('vendor', 'assets', 'css', 'plugins', 'colorpicker')
Rails.application.config.assets.paths << Rails.root.join('vendor', 'assets', 'css', 'plugins', 'chartist')
Rails.application.config.assets.paths << Rails.root.join('vendor', 'assets', 'font-awesome', 'css')
Rails.application.config.assets.paths << Rails.root.join('vendor', 'assets', 'font-awesome', 'fonts')
Rails.application.config.assets.paths << Rails.root.join('vendor', 'assets', 'font-awesome', 'less')
Rails.application.config.assets.paths << Rails.root.join('vendor', 'assets', 'font-awesome', 'scss')
Rails.application.config.assets.paths << Rails.root.join('vendor', 'assets', 'fonts')
Rails.application.config.assets.paths << Rails.root.join('vendor', 'assets', 'img')
Rails.application.config.assets.paths << Rails.root.join('vendor', 'assets', 'img', 'bootstrap-colorpicker')
Rails.application.config.assets.paths << Rails.root.join('vendor', 'assets', 'img', 'landing')
Rails.application.config.assets.paths << Rails.root.join('vendor', 'assets', 'js')
Rails.application.config.assets.paths << Rails.root.join('vendor', 'assets', 'js', 'plugins', 'metisMenu')
Rails.application.config.assets.paths << Rails.root.join('vendor', 'assets', 'js', 'plugins', 'pace')
Rails.application.config.assets.paths << Rails.root.join('vendor', 'assets', 'js', 'plugins', 'slimscroll')
Rails.application.config.assets.paths << Rails.root.join('vendor', 'assets', 'js', 'plugins', 'wow')
Rails.application.config.assets.paths << Rails.root.join('vendor', 'assets', 'js', 'plugins', 'toastr')
Rails.application.config.assets.paths << Rails.root.join('vendor', 'assets', 'js', 'plugins', 'chartJs')
Rails.application.config.assets.paths << Rails.root.join('vendor', 'assets', 'js', 'plugins', 'bootstrap-tagsinput')
Rails.application.config.assets.paths << Rails.root.join('vendor', 'assets', 'js', 'plugins', 'colorpicker')
Rails.application.config.assets.paths << Rails.root.join('vendor', 'assets', 'js', 'plugins', 'chartist')

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in the app/assets
# folder are already added.
Rails.application.config.assets.precompile += %w( *.css *.min.js *.js *.png *.jpg )
