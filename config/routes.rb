# frozen_string_literal: true

Rails.application.routes.draw do
  resources :charts, except: :show do
    resources :center_values, only: :update
    resources :x_values, only: :create
    resources :data_centers, except: %i[index show]
  end
  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations',
    passwords: 'users/passwords'
  }

  authenticated :user do
    root 'charts#index', as: 'authenticated_root'
  end

  root 'dashboard#index'
end
